package model.logic;

import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import model.data_structures.LinkedList;
import model.vo.VOMovingViolation;



public class MovingViolations{

	private LinkedList<VOMovingViolation> moving;
	
	
	private static final String[] attributes = {"OBJECTID","ROW_","LOCATION","ADDRESS_ID","STREETSEGID","XCOORD","YCOORD","TICKETTYPE","FINEAMT","TOTALPAID","PENALTY1","AGENCYID", "PENALTY2","ACCIDENTINDICATOR","TICKETISSUEDATE","VIOLATIONCODE","VIOLATIONDESC","ROW_ID"};


	public MovingViolations()
	{
		moving = new LinkedList<VOMovingViolation>();	
		
	}
	

	public int loadMovingViolations(String movingViolationsFile) {

		int resp = 0;
			try {
				Reader reader = Files.newBufferedReader(Paths.get(movingViolationsFile));
				CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);
				
				
	           int linea = 0;
				for (CSVRecord csvRecord : csvParser) {
					linea++;
					
					if (linea == 1)
					continue;
					
					
					// Accessing Values by Column Index
					String OBJECTID =  csvRecord.get(0);
					
					//String ROW_ = csvRecord.get(1);
					
					String LOCATION = csvRecord.get(2);
					
					/*String ADDRESS_ID = csvRecord.get(3);
				
					String STRRETSEGID = csvRecord.get(4);
					
					String XCOORD = csvRecord.get(5);
					
					String YCOORD = csvRecord.get(6);
					
					String TICKETTYPE = csvRecord.get(7);
					
					*/String FINEAMT = csvRecord.get(8);
					
					String TOTALPAID = csvRecord.get(9);
					
					//String PENALTY1 = csvRecord.get(10);
					
					//String PENALTY2 = csvRecord.get(11);
					
					boolean ACCIDENTINDICATOR = csvRecord.get(12).equals("No")? false:true;
					
					//String AGENCYID = csvRecord.get(13);
					
					String TICKETISSUEDATE = csvRecord.get(14);
					
					//String VIOLATIONCODE = csvRecord.get(15);
					
					String VIOLATIONDESC = csvRecord.get(16);
					
					//String ROWId = csvRecord.get(17);

					
					VOMovingViolation listaa = new VOMovingViolation(LOCATION, TICKETISSUEDATE, VIOLATIONDESC, ACCIDENTINDICATOR, Long.parseLong(OBJECTID), Integer.parseInt(TOTALPAID));
					
					
					moving.add(listaa);
					
					resp =  moving.getSize();

	

				}

			}
			catch (Exception e)
			{
				System.out.println(e.getStackTrace());
			System.out.println(e.getMessage());	
			}
			return resp;
		}
		
		
	

	
	
	
	
}
