package model.data_structures;

public interface ILinkedList<T> extends    Iterable<T>  {

	
	// -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------
	
	/**
     * avanza al siguiente elemento, si existe.
     */
	public T next();
	
	/**
     * retrocede al anterior elemento.
     */
	public T previus();
	
    /**
     * da el tamaño de la lista.
     */
	public int getSize();

	/**
     * da el elemento actual
     */
	public T getCurrent();
	
	/**
     * elmina un elemento de la lista.
     */
	public boolean delete( T elem);
	
	/**
     * agrega un elemento a la lista
     */
	public boolean add(T elem);
	
	/**
     * agrega al final de la lista un elemento
     */
	public boolean addAtEnd(T elem);
	
	/**
     * agrega un elemento en la poscion i 
     */
	public boolean addAtK(T elem, int i);
	
	/**
     * obtiene un elemento de la lista dada la poscion.
     */
	public T getElement(int pPosicion);
	
	/**
     * da el tamaño de la lista.
     */
	public boolean deleteAtK(int i);
	
	
	
}