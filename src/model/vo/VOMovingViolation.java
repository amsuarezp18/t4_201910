package model.vo;

import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Representation of a MovingViolation object
 */
public class VOMovingViolation implements Comparable<VOMovingViolation> {

	// TODO Definir los atributos de una infraccion
	private String location  , violationDesc ; 
	private boolean accidentIndicator ;
	private long id; 
	private int totalPaid; 
	private Date ticketIssue;

	/**
	 * Metodo constructor
	 */
	
	public VOMovingViolation(String plocation, String pticketIssue, String pviolationDesc, boolean paccidentIndicator,
			long pid, int ptotalPaid) {
		super();
		this.location = plocation;
		DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		DateTime dt = dtf.parseDateTime(pticketIssue);
		Date date = dt.toDate();
		this.ticketIssue = date;
		this.violationDesc = pviolationDesc;
		this.accidentIndicator = paccidentIndicator;
		this.id = pid;
		this.totalPaid = ptotalPaid;
	}

	/**
	 * @return id - Identificador unico de la infraccion
	 */
	public long objectId() {
		// TODO Auto-generated method stub
		return id;
	}	
	
	
	/**
	 * @return location - Direccion en formato de texto.
	 */
	public String getLocation() {
		// TODO Auto-generated method stub
		return location; 
	}

	/**
	 * @return date - Fecha cuando se puso la infraccion .
	 */
	public Date getTicketIssueDate() {
		// TODO Auto-generated method stub
		return ticketIssue;
	}
	
	/**
	 * @return totalPaid - Cuanto dinero efectivamente paga el que recibio la infraccion en USD.
	 */
	public int getTotalPaid() {
		// TODO Auto-generated method stub
		return totalPaid;
	}
	
	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public boolean  getAccidentIndicator() {
		// TODO Auto-generated method stub
		return accidentIndicator;
	}
		
	/**
	 * @return description - Descripcion textual de la infraccion.
	 */
	public String  getViolationDescription() {
		// TODO Auto-generated method stub
		return violationDesc;
	}

	@Override
	public int compareTo(VOMovingViolation o) {
		
		
		return o.getTicketIssueDate().getTime() == this.getTicketIssueDate().getTime() ? 0 :  o.getTicketIssueDate().getTime() < this.getTicketIssueDate().getTime() ? 1: -1;
	}
	
	public String toString()
	{
		// TODO Convertir objeto en String (representacion que se muestra en la consola)
		return "VOMovingViolation : id : " + id + ", location : " + location + " , ticketIssue :" + ticketIssue + " ,  total paid : " + totalPaid + " , Accidente Indicator : " + accidentIndicator + " , violationDes : " + violationDesc  ;
	}
}
